import 'package:flutter/material.dart';

const defaultPadding = 20.0;
const cartBarHeight = 100.0;
const headerHeight = 82.0;

const bgColor = Color(0xFFE3EAEA);
const primaryColor = Color(0xFFA6D98F);
const fontColor = Color(0xFF590202);

const panelTransition = Duration(milliseconds: 500);

const kHintTextStyle = TextStyle(
  color: Color(0xFF590202),
);

final kBoxDecorationStyle = BoxDecoration(
  color: const Color(0xFFA6D98F),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: const [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

final RegExp emailValidatorRegExp = RegExp(
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
final RegExp nameValidatorRegExp = RegExp(r'^[a-z A-Z,.\-]+$');
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kInvalidFirstNameError = "Please Enter Valid firstname";
const String kInvalidLastNameError = "Please Enter Valid lastname";
const String kPassNullError = "Please Enter your password";
const String kLastNameNullError = "Please Enter your lastname";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your firstname";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";

const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const headingStyle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: fontColor,
  height: 1.5,
);

final otpInputDecoration = InputDecoration(
  contentPadding: const EdgeInsets.symmetric(vertical: 15),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(15),
    borderSide: const BorderSide(color: kTextColor),
  );
}
