// import 'package:flutter/material.dart';
// import 'package:lunarcoding/screens/profile/components/profile_menu.dart';
// import 'package:lunarcoding/screens/profile/components/profile_picture.dart';

// // class ProfileBody extends StatelessWidget {
// //   const ProfileBody({Key? key}) : super(key: key);

// //   @override
// //   Widget build(BuildContext context) {
// //     return Column(
// //       children: [
// //         const ProfilePicture(),
// //         const SizedBox(height: 20),
// //         ClipRRect(
// //           borderRadius: BorderRadius.circular(4),
// //           child: Stack(
// //             children: <Widget>[
// //               Positioned.fill(
// //                 child: Container(
// //                   width: double.infinity,
// //                   decoration: const BoxDecoration(
// //                     color: Color(0xFFFFFFFF),
// //                   ),
// //                 ),
// //               ),
// //               TextButton(
// //                 style: TextButton.styleFrom(
// //                   primary: Colors.amber,
// //                   textStyle: const TextStyle(fontSize: 20),
// //                 ),
// //                 onPressed: () {},
// //                 child: const Text('Gradient'),
// //               ),
// //             ],
// //           ),
// //         ),
// //       ],
// //     );
// //   }
// // }


// class ProfileBody extends StatelessWidget {
//   const ProfileBody({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       padding: const EdgeInsets.symmetric(vertical: 20),
//       child: Column(
//         children: [
//           const ProfilePicture(),
//           const SizedBox(height: 20),
//           ProfileMenu(
//             text: "My Account",
//             icon: "assets/icons/heart.svg",
//             press: () => {},
//           ),
//           ProfileMenu(
//             text: "Notifications",
//             icon: "assets/icons/heart.svg",
//             press: () {},
//           ),
//           ProfileMenu(
//             text: "Settings",
//             icon: "assets/icons/heart.svg",
//             press: () {},
//           ),
//           ProfileMenu(
//             text: "Help Center",
//             icon: "assets/icons/heart.svg",
//             press: () {},
//           ),
//           ProfileMenu(
//             text: "Log Out",
//             icon: "assets/icons/heart.svg",
//             press: () {},
//           ),
//         ],
//       ),
//     );
//   }
// }