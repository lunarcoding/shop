import 'package:flutter/material.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';


class Payment extends StatelessWidget {
  const Payment({Key? key,required this.bloc}) : super(key: key);
final HomeBLoC bloc;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            const Text(
              "Payment Type :",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            const SizedBox(width: 10),
            Image.asset("assets/images/639365.png"),
            const SizedBox(width: 10),
            const Text(
              "CASH",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            const Expanded(
              child: Text(
                "Edit",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        const SizedBox(height: 15),
        Image.asset("assets/images/Line 2.png"),
        Row(
          children:  [
            const SizedBox(width: 10),
            const Text(
              "Total",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
               bloc.totalCartCost().toString(),
                style: const TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        
      ],
    );
  }
}
