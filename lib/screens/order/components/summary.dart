import 'package:flutter/material.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/constants.dart';

class Summary extends StatelessWidget {
  const Summary({Key? key, required this.bloc,}) : super(key: key);
 final HomeBLoC bloc;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: const [
            Text(
              "Summary",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "Edit",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        const SizedBox(height: 10),
        ...List.generate(
            bloc.cart.length,
            (index) => Row(
          children:  [
            const SizedBox(width: 10),
            Text(
              bloc.cart[index].product!.title!,
              style: const TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            const SizedBox(width: 10),
            Text(
              "  x ${bloc.cart[index].quantity}",
              style: const TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "${bloc.cart[index].product!.cost}",
                style: const TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
          ),
        const SizedBox(height: 15),
        Image.asset("assets/images/Line 2.png"),
        Row(
          children:  [
            const SizedBox(width: 10),
            const Text(
              "subtotal",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                bloc.totalCartCost().toString(),
                style: const TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "delivery fee",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "0",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "Coupon:",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            SizedBox(width: 10),
            Text(
              "",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "0",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          children: const [
            SizedBox(width: 10),
            Text("you save : ",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right),
            SizedBox(width: 10),
            Text(
              "0",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
              textAlign: TextAlign.right,
            ),
          ],
        ),
      ],
    );
  }
}
