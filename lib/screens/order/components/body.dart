import 'package:flutter/material.dart';
import 'package:lunarcoding/components/default_button.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/screens/order/components/location.dart';
import 'package:lunarcoding/screens/order/components/payment.dart';
import 'package:lunarcoding/screens/order/components/summary.dart';
import 'package:lunarcoding/screens/order/submitscreen.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decode/jwt_decode.dart';
import 'dart:convert';

Future<Home> fetchHome(String token) async {
  var id = Jwt.parseJwt(token);
  var userid = ID.fromJson(id).user_id;
  final response =
      await http.get(Uri.parse('http://192.168.1.34:8080/users/$userid')
          // Send authorization headers to the backend.
          );
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Home.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load Home');
  }
}

class ID {
  // ignore: non_constant_identifier_names
  final int user_id;

  // ignore: non_constant_identifier_names
  ID({required this.user_id});

  factory ID.fromJson(Map<String, dynamic> json) {
    return ID(
      user_id: json['user_id'],
    );
  }
}

class Home {
  final int id;
  final String first_name;
  final String sur_name;
  final String phone;
  final String address;
  final String email;

  Home({
    required this.id,
    required this.first_name,
    required this.sur_name,
    required this.phone,
    required this.address,
    required this.email,
  });

  factory Home.fromJson(Map<String, dynamic> json) {
    return Home(
      id: json['id'],
      first_name: json['first_name'],
      sur_name: json['sur_name'],
      phone: json['phone'],
      address: json['address'],
      email: json['email'],
    );
  }
}

class Body extends StatefulWidget {
  const Body({Key? key, required this.token, required this.bloc,}) : super(key: key);
  final String token;
  final HomeBLoC bloc;

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<Home> futureHome;

  @override
  void initState() {
    super.initState();
    futureHome = fetchHome(widget.token);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      backgroundColor: bgColor,
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(right: 30, left: 30, bottom: 50),
        child: NeumorphicButton(
          style: NeumorphicStyle(
            boxShape: NeumorphicBoxShape.roundRect(
              BorderRadius.circular(30),
            ),
            color: primaryColor,
            shape: NeumorphicShape.convex,
            depth: 10,
            intensity: 0.81,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SubmitScreen(token: widget.token)));
          },
          child: const Text(
            "Continue",
            textAlign: TextAlign.center,
            style: TextStyle(color: fontColor, fontSize: 30),
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/Group 44.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: FutureBuilder<Home>(
                future: futureHome,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        Location(location: snapshot.data!.address),
                         Summary(bloc:widget.bloc),
                         Payment(bloc:widget.bloc),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }

                  // By default, show a loading spinner.
                  return const CircularProgressIndicator();
                }),
          ),
        ),
      ),
    );
  }
}

AppBar buildAppBar() {
  return AppBar(
    leading: const BackButton(
      color: fontColor,
    ),
    backgroundColor: bgColor,
    elevation: 0,
    centerTitle: true,
    title: const Text(
      "Vegetable",
      style: TextStyle(color: fontColor),
    ),
    actions: [
      Image.asset(
        "assets/images/profile.png",
        height: 40,
        width: 40,
      ),
      SizedBox(width: defaultPadding),
    ],
  );
}
