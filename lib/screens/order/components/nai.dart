import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

class Nai extends StatelessWidget {
  const Nai({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            Image.asset("assets/images/684908.png"),
            const SizedBox(width: 15),
            const Text(
              "55/55 Too road",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
          ],
        ),
        const SizedBox(height: 5),
        Row(
          children: const [
            Text(
              "Summary",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "Edit",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "Cabbage",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            SizedBox(width: 10),
            Text(
              "3x",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "60",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        const SizedBox(height: 15),
        Image.asset("assets/images/Line 2.png"),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "subtotal",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "100",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "delivery fee",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "5",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "Coupon:",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            SizedBox(width: 10),
            Text(
              "Have a khaikem day",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "-9",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          children: const [
            SizedBox(width: 10),
            Text("you save : ",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right),
            SizedBox(width: 10),
            Text(
              "9",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
              textAlign: TextAlign.right,
            ),
          ],
        ),
        const SizedBox(height: 5),
        Row(
          children: [
            const Text(
              "Payment Type :",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            const SizedBox(width: 10),
            Image.asset("assets/images/639365.png"),
            const SizedBox(width: 10),
            const Text(
              "CASH",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            const Expanded(
              child: Text(
                "Edit",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        const SizedBox(height: 15),
        Image.asset("assets/images/Line 2.png"),
        Row(
          children: const [
            SizedBox(width: 10),
            Text(
              "Total",
              style: TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
            Expanded(
              child: Text(
                "100",
                style: TextStyle(
                  fontSize: 18,
                  color: fontColor,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),

      
      ],
    );
  }
}
