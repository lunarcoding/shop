import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

class Location extends StatelessWidget {
  const Location({Key? key, required this.location}) : super(key: key);
  final String location;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            Image.asset("assets/images/684908.png"),
            const SizedBox(width: 15),
            Text(
              location,
              style: const TextStyle(
                fontSize: 18,
                color: fontColor,
              ),
            ),
          ],
        ),
        // const Text(
        //   "chaingrai , naiwieng , banki...",
        //   style: TextStyle(
        //     fontSize: 18,
        //     color: fontColor,
        //   ),
        // ),
        // const SizedBox(height: 10),
        // const Text(
        //   "description",
        //   style: TextStyle(
        //     fontSize: 18,
        //     color: fontColor,
        //   ),
        // ),
        // const SizedBox(height: 5),
        // Padding(
        //   padding: const EdgeInsets.only(right: 20, left: 20),
        //   child: Row(
        //     children: const [
        //       Text(
        //         "Green House \nchicken attacking the wall",
        //         style: TextStyle(
        //           fontSize: 18,
        //           color: fontColor,
        //         ),
        //       ),
        //       Expanded(
        //         child: Text(
        //           "edit",
        //           style: TextStyle(
        //             fontSize: 18,
        //             color: fontColor,
        //           ),
        //           textAlign: TextAlign.right,
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
