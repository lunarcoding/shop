import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/screens/home/home_screen.dart';

class SubmitScreen extends StatelessWidget {
  const SubmitScreen({Key? key, required this.token}) : super(key: key);
  final String token;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/Group 44.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/logo/logo.png"),
            const SizedBox(height: 5),
            const Text(
              "Order Complete",
              style: TextStyle(
                fontSize: 36,
                color: fontColor,
              ),
            ),
            const SizedBox(height: 15),
            NeumorphicButton(
              style: NeumorphicStyle(
                boxShape: NeumorphicBoxShape.roundRect(
                  BorderRadius.circular(30),
                ),
                color: primaryColor,
                shape: NeumorphicShape.convex,
                depth: 10,
                intensity: 0.81,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => HomeScreen(token: token)));
              },
              child: const Text(
                "Continue",
                textAlign: TextAlign.center,
                style: TextStyle(color: fontColor, fontSize: 30),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
