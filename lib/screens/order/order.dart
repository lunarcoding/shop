import 'package:flutter/material.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/components/navigation_drawer_widget.dart';
import 'package:lunarcoding/screens/order/components/body.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({Key? key, required this.token,    required this.bloc,}) : super(key: key);
  final String token;
    final HomeBLoC bloc;
  @override
  Widget build(BuildContext context) {
    return Scaffold(endDrawer: NavigationDrawerWidget(token:token), body:  Body(token:token,bloc:bloc));
  }
}
