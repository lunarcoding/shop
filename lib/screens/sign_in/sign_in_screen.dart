import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

import 'components/body.dart';

class SignInScreen extends StatelessWidget {
  // static String routeName = "/sign_in";

  const SignInScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: bgColor,
      body: Body(),
    );
  }
}
