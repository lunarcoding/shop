import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/components/no_account_text.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/screens/sign_in/components/sign_form.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/Group 44.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset("assets/logo/logo.png"),
                      const SizedBox(width: 10),
                      Column(
                        children: [
                          Text(
                            "K H A I P U K",
                            style: GoogleFonts.ptSans(
                              textStyle: const TextStyle(
                                color: fontColor,
                                fontSize: 30,
                              ),
                            ),
                          ),
                          Text(
                            "Powered By\nLunar Coding",
                            style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                color: fontColor,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  const SignForm(),
                  const SizedBox(height: 20),
                  const NoAccountText(),
                  const SizedBox(
                    height: 30,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
