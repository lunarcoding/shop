import 'package:flutter/material.dart';
import 'package:lunarcoding/components/default_button.dart';
import 'package:lunarcoding/components/form_error.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/helper/keyboard.dart';
import 'package:lunarcoding/screens/forgot_password/forgot_password_screen.dart';
import 'package:lunarcoding/screens/login_success/login_success_screen.dart';
import 'package:lunarcoding/src/widget/container.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:jwt_decode/jwt_decode.dart';

Future<Login> createLogin(String email, String password, context) async {
  final response = await http.post(
    Uri.parse('http://192.168.1.34:8080/login'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'email': email,
      'password': password,
    }),
  );

  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    // print("OK");
    // return Login.fromJson(jsonDecode(response.body));
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                LoginSuccessScreen(token: jsonDecode(response.body))));
    return Login.fromJson(Jwt.parseJwt(jsonDecode(response.body)));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create Login.');
  }
}

class Login {
  final int user_id;

  Login({required this.user_id});

  factory Login.fromJson(Map<String, dynamic> json) {
    return Login(
      user_id: json['user_id'],
    );
  }
}

class SignForm extends StatefulWidget {
  const SignForm({Key? key}) : super(key: key);

  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  Future<Login>? _futureLogin;

  final _formKey = GlobalKey<FormState>();
  String? email;
  String? password;
  bool? remember = false;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          const SizedBox(height: 30),
          buildPasswordFormField(),
          const SizedBox(height: 30),
          Row(
            children: [
              Checkbox(
                value: remember,
                activeColor: fontColor,
                onChanged: (value) {
                  setState(() {
                    remember = value;
                  });
                },
              ),
              const Text(
                "Remember me",
                style: TextStyle(color: fontColor),
              ),
              const Spacer(),
              GestureDetector(
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ForgotPasswordScreen())),
                child: const Text(
                  "Forgot Password",
                  style: TextStyle(
                      decoration: TextDecoration.underline, color: fontColor),
                ),
              )
            ],
          ),
          FormError(errors: errors),
          const SizedBox(height: 20),
          SizedBox(
            width: 200,
            child: NeumorphicButton(
              style: NeumorphicStyle(
                boxShape: NeumorphicBoxShape.roundRect(
                  BorderRadius.circular(30),
                ),
                color: primaryColor,
                shape: NeumorphicShape.convex,
                depth: 10,
                intensity: 0.81,
              ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  // if all are valid then go to success screen
                  KeyboardUtil.hideKeyboard(context);
                  // Navigator.pushNamed(context, LoginSuccessScreen.routeName);
                  setState(() {
                    _futureLogin =
                        createLogin(_email.text, _password.text, context);
                  });
                }
              },
              child: const Text(
                "Continue",
                textAlign: TextAlign.center,
                style: TextStyle(color: fontColor, fontSize: 30),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column buildPasswordFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          "Password",
          style: TextStyle(color: fontColor),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _password,
            obscureText: true,
            onSaved: (newValue) => password = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kPassNullError);
              } else if (value.length >= 8) {
                removeError(error: kShortPassError);
              }
              // ignore: avoid_returning_null_for_void
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kPassNullError);
                return kPassNullError;
              } else if (value.length < 8) {
                addError(error: kShortPassError);
                return kShortPassError;
              }
              return null;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
                hintText: "Enter your password",
                hintStyle: const TextStyle(color: fontColor),
                // If  you are using latest version of flutter then lable text and hint text shown like this
                // if you r using flutter less then 1.20.* then maybe this is not working properly

                suffixIcon: const Icon(
                  Icons.lock_outlined,
                  color: fontColor,
                ),
                contentPadding: const EdgeInsets.only(
                  left: 15,
                )),
          ),
        ),
      ],
    );
  }

  Column buildEmailFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          "Email",
          style: TextStyle(color: fontColor),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _email,
            keyboardType: TextInputType.emailAddress,
            onSaved: (newValue) => email = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kEmailNullError);
              } else if (emailValidatorRegExp.hasMatch(value)) {
                removeError(error: kInvalidEmailError);
              }
              // ignore: avoid_returning_null_for_void
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kEmailNullError);
                return kEmailNullError;
              } else if (!emailValidatorRegExp.hasMatch(value)) {
                addError(error: kInvalidEmailError);
                return kInvalidEmailError;
              }
              return null;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
                hintText: "Enter your email",
                hintStyle: const TextStyle(color: fontColor),
                // If  you are using latest version of flutter then lable text and hint text shown like this
                // if you r using flutter less then 1.20.* then maybe this is not working properly
                floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon: const Icon(
                  Icons.email_outlined,
                  color: fontColor,
                ),
                contentPadding: const EdgeInsets.only(
                  left: 15,
                )),
          ),
        ),
      ],
    );
  }
}
