import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

import 'components/body.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  // static String routeName = "/forgot_password";
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      
      backgroundColor: bgColor,
      body: Body(),
    );
  }
}
