import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/components/custom_surfix_icon.dart';
import 'package:lunarcoding/components/default_button.dart';
import 'package:lunarcoding/components/form_error.dart';
import 'package:lunarcoding/components/no_account_text.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/screens/sign_in/sign_in_screen.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/Group 44.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                const SizedBox(height: 50),
                // ignore: unnecessary_const
                Text(
                  "Forgot Password",
                  style: GoogleFonts.ptSans(
                    textStyle: const TextStyle(
                      fontSize: 28,
                      color: fontColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  "Please enter your email and we will send \nyou a link to return to your account",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.ptSans(color: fontColor),
                ),
                const SizedBox(height: 5),
                const ForgotPassForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ForgotPassForm extends StatefulWidget {
  const ForgotPassForm({Key? key}) : super(key: key);

  @override
  _ForgotPassFormState createState() => _ForgotPassFormState();
}

class _ForgotPassFormState extends State<ForgotPassForm> {
  final _formKey = GlobalKey<FormState>();
  List<String> errors = [];
  String? email;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Email",
            style: GoogleFonts.ptSans(
                textStyle: const TextStyle(color: fontColor)),
          ),
          const SizedBox(
            height: 10,
          ),
          Neumorphic(
            style: NeumorphicStyle(
              color: bgColor,
              shape: NeumorphicShape.concave,
              boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
              depth: -8,
            ),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              onSaved: (newValue) => email = newValue,
              onChanged: (value) {
                if (value.isNotEmpty && errors.contains(kEmailNullError)) {
                  setState(() {
                    errors.remove(kEmailNullError);
                  });
                } else if (emailValidatorRegExp.hasMatch(value) &&
                    errors.contains(kInvalidEmailError)) {
                  setState(() {
                    errors.remove(kInvalidEmailError);
                  });
                }
                return null;
              },
              validator: (value) {
                if (value!.isEmpty && !errors.contains(kEmailNullError)) {
                  setState(() {
                    errors.add(kEmailNullError);
                  });
                } else if (!emailValidatorRegExp.hasMatch(value) &&
                    !errors.contains(kInvalidEmailError)) {
                  setState(() {
                    errors.add(kInvalidEmailError);
                  });
                }
                return null;
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
                hintText: "Enter your email",
                hintStyle: GoogleFonts.ptSans(
                    textStyle: const TextStyle(color: fontColor)),
                // If  you are using latest version of flutter then lable text and hint text shown like this
                // if you r using flutter less then 1.20.* then maybe this is not working properly
                floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon: const Icon(
                  Icons.email_outlined,
                  color: fontColor,
                ),
                contentPadding: const EdgeInsets.only(
                  left: 15,
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          FormError(errors: errors),
          const SizedBox(height: 5),
          SizedBox(
            width: 400,
            child: NeumorphicButton(
              style: NeumorphicStyle(
                boxShape: NeumorphicBoxShape.roundRect(
                  BorderRadius.circular(30),
                ),
                color: primaryColor,
                shape: NeumorphicShape.convex,
                depth: 10,
                intensity: 0.81,
              ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SignInScreen()));
                }
              },
              child: Text(
                "Continue",
                textAlign: TextAlign.center,
                style: GoogleFonts.ptSans(
                  textStyle: const TextStyle(
                    color: fontColor,
                    fontSize: 30,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          const NoAccountText(),
        ],
      ),
    );
  }
}
