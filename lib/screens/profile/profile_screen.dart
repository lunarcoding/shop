import 'package:flutter/material.dart';
import 'package:lunarcoding/components/navigation_drawer_widget.dart';
import 'package:lunarcoding/screens/profile/components/body.dart';

import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  // static String routeName = "/profile";

  const ProfileScreen({Key? key, required this.token}) : super(key: key);
  final String token;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      endDrawer: NavigationDrawerWidget(token:token),
      body: Body(token:token),
      // bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
