import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';

class ProfileName extends StatelessWidget {
  const ProfileName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 20),
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            "Name",
            style: GoogleFonts.roboto(
              textStyle: const TextStyle(
                color: fontColor,
                fontSize: 24,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ProfileMobileNumber extends StatelessWidget {
  const ProfileMobileNumber({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 20),
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            "Mobile Number",
            style: GoogleFonts.roboto(
              textStyle: const TextStyle(
                color: fontColor,
                fontSize: 24,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ProfileEmail extends StatelessWidget {
  const ProfileEmail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 20),
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            "Email Address",
            style: GoogleFonts.roboto(
              textStyle: const TextStyle(
                color: fontColor,
                fontSize: 24,
              ),
            ),
          ),
        ),
      ],
    );
  }
}