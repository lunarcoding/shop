import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

class ProfileTextFormFieldName extends StatelessWidget {
  ProfileTextFormFieldName({Key? key, required this.name}) : super(key: key);
  final String name;
  @override
  Widget build(BuildContext context) {
    TextEditingController myController = TextEditingController()..text = name;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Container(
        alignment: Alignment.centerLeft,
        decoration: kBoxDecorationStyle,
        height: 60.0,
        child: TextField(
          controller: myController,
          style: const TextStyle(
            color: Colors.white,
            fontFamily: 'OpenSans',
          ),
          decoration: const InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.only(top: 14.0, left: 10),
            // suffixIcon: Icon(
            //   Icons.edit,
            //   color: Colors.white,
            // ),
          ),
        ),
      ),
    );
  }
}

class ProfileTextFormFieldMobileNumber extends StatelessWidget {
  ProfileTextFormFieldMobileNumber({Key? key, required this.phone})
      : super(key: key);
  final String phone;
  @override
  Widget build(BuildContext context) {
    TextEditingController myController = TextEditingController()..text = phone;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Container(
        alignment: Alignment.centerLeft,
        decoration: kBoxDecorationStyle,
        height: 60.0,
        child: TextField(
          controller: myController,
          style: const TextStyle(
            color: Colors.white,
            fontFamily: 'OpenSans',
          ),
          decoration: const InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.only(top: 14.0, left: 10),
            // suffixIcon: Icon(
            //   Icons.edit,
            //   color: Colors.white,
            // ),
          ),
        ),
      ),
    );
  }
}

class ProfileTextFormFieldEmail extends StatelessWidget {
  const ProfileTextFormFieldEmail({Key? key, required this.email})
      : super(key: key);
  final String email;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Container(
        alignment: Alignment.centerLeft,
        decoration: kBoxDecorationStyle,
        height: 60.0,
        child: TextFormField(
          initialValue: email,
          style: const TextStyle(
            color: Colors.white,
            fontFamily: 'OpenSans',
          ),
          decoration: const InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.only(top: 14.0, left: 10),
            // suffixIcon: Icon(
            //   Icons.edit,
            //   color: Colors.white,
            // ),
          ),
        ),
      ),
    );
  }
}
