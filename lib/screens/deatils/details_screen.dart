import 'package:lunarcoding/components/fav_btn.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/components/navigation_drawer_widget.dart';
import 'package:lunarcoding/components/price.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/models/Product.dart';
import 'package:flutter/material.dart';

import 'components/button_cart.dart';
import 'components/cart_counter.dart';

class DetailsScreen extends StatefulWidget {
  const DetailsScreen({
    Key? key,
    required this.product,
    required this.bloc,
    required this.token,
  }) : super(key: key);

  final Product product;
  final HomeBLoC bloc;
  final String token;

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  int amount = 1;

  String _cartTag = "";

  void _addToCart(BuildContext context) {
    widget.bloc.addProductToCart(widget.product, amount);
    setState(() {
      _cartTag = "_cart";
    });
    Navigator.pop(context);
  }

  void _addamountcounter() {
    setState(() {
      amount++;
    });
  }

  void _removeamountcounter() {
    setState(() {
      amount--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: NavigationDrawerWidget(token:widget.token),
      bottomNavigationBar: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
          child: SizedBox(
            width: double.infinity,
            child: GestureDetector(
                // FIRST BUTTON
                onTap: () => _addToCart(context),
                child: const ButtonCart()),
          ),
        ),
      ),
      backgroundColor: bgColor,
      appBar: buildAppBar(),
      body: SingleChildScrollView(
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/Group 44.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              AspectRatio(
                aspectRatio: 1.37,
                child: Stack(
                  clipBehavior: Clip.none,
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      color: bgColor,
                      child: Hero(
                        tag: widget.product.title! + _cartTag,
                        child: Image.asset(widget.product.image!),
                      ),
                    ),
                    Positioned(
                      bottom: -20,
                      child: CartCounter(
                        amount: amount,
                        plus: _addamountcounter,
                        remove: _removeamountcounter,
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: defaultPadding * 1.5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        widget.product.title!,
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              fontWeight: FontWeight.bold,
                              color: fontColor,
                            ),
                      ),
                    ),
                    Price(amount: widget.product.cost.toString()),
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(defaultPadding),
                child: Text(
                  "Cabbage (comprising several cultivars of Brassica oleracea) is a leafy green, red (purple), or white (pale green) biennial plant grown as an annual vegetable crop for its dense-leaved heads. It is descended from the wild cabbage (B. oleracea var. oleracea), and belongs to the cole crops or brassicas, meaning it is closely related to broccoli and cauliflower (var. botrytis); Brussels sprouts (var. gemmifera); and Savoy cabbage (var. sabauda).",
                  style: TextStyle(
                    color: Color(0xFFB4B4B4),
                    height: 1.8,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      leading: const BackButton(
        color: fontColor,
      ),
      backgroundColor: bgColor,
      elevation: 0,
      centerTitle: true,
      title: const Text(
        "Vegetable",
        style: TextStyle(color: fontColor),
      ),
      actions: const [
        FavBtn(radius: 20),
        SizedBox(width: defaultPadding),
      ],
    );
  }
}
