import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

class ButtonCart extends StatelessWidget {
  const ButtonCart({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        width: double.infinity,
        height: 60,
        alignment: Alignment.center,
        child: const Text(
          "Add to Basket",
          textAlign: TextAlign.center,
          style: TextStyle(color: fontColor, fontSize: 30),
        ),
        decoration: const BoxDecoration(
          color: primaryColor,
          borderRadius: BorderRadius.all(
            Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
                color: Color(0xFF9E9E9E),
                offset: Offset(5.0, 5.0),
                blurRadius: 15.0,
                spreadRadius: 1.0),
            BoxShadow(
                color: Colors.white,
                offset: Offset(-5.0, -5.0),
                blurRadius: 15.0,
                spreadRadius: 1.0),
          ],
        ),
      ),
    );
  }
}
