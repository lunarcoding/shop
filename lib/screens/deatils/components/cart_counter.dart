import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:lunarcoding/screens/deatils/components/rounded_icon_btn.dart';

import '../../../constants.dart';

class CartCounter extends StatefulWidget {
  CartCounter(
      {Key? key,
      required this.amount,
      required this.plus,
      required this.remove})
      : super(key: key);

  final int amount;
  final VoidCallback plus;
  final VoidCallback remove;
  @override
  State<CartCounter> createState() => _CartCounterState();
}

class _CartCounterState extends State<CartCounter> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color(0xFFF6F6F6),
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Row(
        children: [
          RoundIconBtn(
            iconData: Icons.remove,
            color: const Color(0xFFB4B4B4),
            press: widget.remove,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: defaultPadding / 4),
            child: Text(
              widget.amount.toString(),
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w800,
                color: fontColor,
              ),
            ),
          ),
          RoundIconBtn(
            iconData: Icons.add,
            color: const Color(0xFF54EE6D),
            press: widget.plus,
          ),
        ],
      ),
    );
  }
}
