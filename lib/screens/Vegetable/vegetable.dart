import 'package:get/get_connect/http/src/request/request.dart';
import 'package:lunarcoding/components/card_shortview.dart';
import 'package:lunarcoding/components/cart_detailsview.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/components/navigation_drawer_widget.dart';
import 'package:lunarcoding/components/product_card_old.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/models/Product.dart';
import 'package:lunarcoding/screens/deatils/details_screen.dart';
import 'package:flutter/material.dart';
import 'package:lunarcoding/screens/home/components/home_header.dart';

class VegetableScreen extends StatefulWidget {
  const VegetableScreen({
    Key? key,
    required this.bloc,
    required this.token,
  }) : super(key: key);
  final HomeBLoC bloc;
  final String token;
  @override
  _VegetableScreenState createState() => _VegetableScreenState();
}

class _VegetableScreenState extends State<VegetableScreen> {
  void _onVerticalGesture(DragUpdateDetails details) {
    if (details.primaryDelta! < -0.7) {
      widget.bloc.changeToCart();
    } else if (details.primaryDelta! > 0.7) {
      widget.bloc.changeToNormal();
    }
  }

  double _getTopForWhitePanel(HomeState state, double maxHeight) {
    if (state == HomeState.normal) {
      return 0;
    } else if (state == HomeState.cart) {
      return -(maxHeight - cartBarHeight * 2);
    }
    return 0;
  }

  double _getTopForCartPanel(HomeState state, double maxHeight) {
    if (state == HomeState.normal) {
      return cartBarHeight;
    } else if (state == HomeState.cart) {
      return maxHeight - cartBarHeight;
    }
    return 0;
  }

  double _headerPosition(HomeState state) {
    if (state == HomeState.normal) {
      return 0;
    } else if (state == HomeState.cart) {
      return -headerHeight;
    }
    return 0;
  }

  int quantity = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: NavigationDrawerWidget(token: widget.token),
      appBar: buildAppBar(),
      body: AnimatedBuilder(
          animation: widget.bloc,
          builder: (context, _) {
            return Scaffold(
              backgroundColor: bgColor,
              body: SafeArea(
                bottom: false,
                child: Container(
                  color: primaryColor,
                  child: Column(
                    children: [
                      Expanded(
                        child: LayoutBuilder(
                          builder: (context, BoxConstraints constraints) {
                            return Stack(
                              children: [
                                // Cart
                                CartNav(constraints),
                                AnimatedPositioned(
                                  duration: panelTransition,
                                  curve: Curves.decelerate,
                                  top: _getTopForWhitePanel(
                                      widget.bloc.homeState,
                                      constraints.maxHeight),
                                  left: 0,
                                  right: 0,
                                  height: constraints.maxHeight - cartBarHeight,
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: defaultPadding),
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/Group 44.png"),
                                        fit: BoxFit.cover,
                                      ),
                                      color: bgColor,
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(
                                            defaultPadding * 1.5),
                                        bottomRight: Radius.circular(
                                            defaultPadding * 1.5),
                                      ),
                                    ),
                                    child: GridView.builder(
                                      itemCount: demo_products.length,
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        childAspectRatio: 0.75,
                                        mainAxisSpacing: defaultPadding,
                                        crossAxisSpacing: defaultPadding,
                                      ),
                                      itemBuilder: (context, index) =>
                                          ProductCard(
                                        product: demo_products[index],
                                        press: () {
                                          Navigator.of(context).push(
                                            PageRouteBuilder(
                                              transitionDuration:
                                                  const Duration(
                                                      milliseconds: 500),
                                              reverseTransitionDuration:
                                                  const Duration(
                                                      milliseconds: 600),
                                              pageBuilder: (context, animation,
                                                      secondaryAnimation) =>
                                                  FadeTransition(
                                                opacity: animation,
                                                child: DetailsScreen(
                                                  product: demo_products[index],
                                                  bloc: widget.bloc,
                                                  token: widget.token,
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  // ignore: non_constant_identifier_names
  AnimatedPositioned CartNav(BoxConstraints constraints) {
    return AnimatedPositioned(
      duration: panelTransition,
      curve: Curves.decelerate,
      bottom: 0,
      left: 0,
      right: 0,
      height: _getTopForCartPanel(widget.bloc.homeState, constraints.maxHeight),
      child: GestureDetector(
        onVerticalDragUpdate: _onVerticalGesture,
        child: Container(
          alignment: Alignment.topCenter,
          color: primaryColor,
          child: Padding(
            padding: const EdgeInsets.all(defaultPadding),
            child: AnimatedSwitcher(
              duration: panelTransition,
              child: widget.bloc.homeState == HomeState.normal
                  ? CartShortView(bloc: widget.bloc)
                  : CartDetailsView(bloc: widget.bloc, token: widget.token),
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      leading: const BackButton(
        color: fontColor,
      ),
      backgroundColor: bgColor,
      elevation: 0,
      centerTitle: true,
      title: const Text(
        "Vegetable",
        style: TextStyle(color: fontColor),
      ),
      actions: [
        Image.asset(
          "assets/images/profile.png",
          height: 40,
          width: 40,
        ),
        SizedBox(width: defaultPadding),
      ],
    );
  }
}
