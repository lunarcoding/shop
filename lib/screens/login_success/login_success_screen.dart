import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

import 'components/body.dart';

class LoginSuccessScreen extends StatelessWidget {
  const LoginSuccessScreen({Key? key, required this.token}) : super(key: key);

  final String token;
  // static String routeName = "/login_success";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Body(token:token),
    );
  }
}
