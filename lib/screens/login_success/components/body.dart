import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/screens/home/home_screen.dart';

class Body extends StatelessWidget {
  const Body({Key? key,required this.token}) : super(key: key);

  final String token;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/Group 44.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: Column(
        children: [
          const SizedBox(height: 12),
          Image.asset(
            "assets/images/success.png",
            // height: 12, //40%
          ),
          const SizedBox(height: 15),
          const Text(
            "Login Success",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          const Spacer(),
          SizedBox(
            width: 400,
            child: NeumorphicButton(
              style: NeumorphicStyle(
                boxShape: NeumorphicBoxShape.roundRect(
                  BorderRadius.circular(30),
                ),
                color: primaryColor,
                shape: NeumorphicShape.convex,
                depth: 10,
                intensity: 0.81,
              ),
              // press: () {
              //   Navigator.pushNamed(context, HomeScreen.routeName);
              // },
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>  HomeScreen(token:token)));
              },
              child: Text(
                "Back to home",
                textAlign: TextAlign.center,
                style: GoogleFonts.ptSans(
                  textStyle: const TextStyle(
                    color: fontColor,
                    fontSize: 30,
                  ),
                ),
              ),
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
