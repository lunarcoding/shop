import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/components/form_error.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/screens/register_profile/complete_profile_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:lunarcoding/screens/sign_in/sign_in_screen.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String? email;
  String? password;
  String? conform_password;
  bool remember = false;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          const SizedBox(height: 30),
          buildPasswordFormField(),
          const SizedBox(height: 30),
          buildConformPassFormField(),
          FormError(errors: errors),
          const SizedBox(height: 40),
          SizedBox(
            width: 200,
            child: NeumorphicButton(
              style: NeumorphicStyle(
                boxShape: NeumorphicBoxShape.roundRect(
                  BorderRadius.circular(30),
                ),
                color: primaryColor,
                shape: NeumorphicShape.convex,
                depth: 10,
                intensity: 0.81,
              ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  // if all are valid then go to success screen
                  // Navigator.pushNamed(context, CompleteProfileScreen.routeName)
                   Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CompleteProfileScreen(
               email:_email.text,password:_password.text)));
                }
              },
              child: Text(
                "Next",
                textAlign: TextAlign.center,
                style: GoogleFonts.ptSans(
                  textStyle: const TextStyle(color: fontColor, fontSize: 30),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column buildConformPassFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Confirm Password",
          style:
              GoogleFonts.ptSans(textStyle: const TextStyle(color: fontColor)),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            obscureText: true,
            onSaved: (newValue) => conform_password = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kPassNullError);
              } else if (value.isNotEmpty && password == conform_password) {
                removeError(error: kMatchPassError);
              }
              conform_password = value;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kPassNullError);
                return kPassNullError;
              } else if ((password != value)) {
                addError(error: kMatchPassError);
                return kMatchPassError;
              }
              return null;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
                hintText: "Re-enter your password",
                hintStyle: GoogleFonts.ptSans(
                    textStyle: const TextStyle(color: fontColor)),
                // If  you are using latest version of flutter then lable text and hint text shown like this
                // if you r using flutter less then 1.20.* then maybe this is not working properly
                floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon: const Icon(
                  Icons.lock_outlined,
                  color: fontColor,
                ),
                contentPadding: const EdgeInsets.only(
                  left: 15,
                )),
          ),
        ),
      ],
    );
  }

  Column buildPasswordFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Password",
          style:
              GoogleFonts.ptSans(textStyle: const TextStyle(color: fontColor)),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _password,
            obscureText: true,
            onSaved: (newValue) => password = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kPassNullError);
              } else if (value.length >= 8) {
                removeError(error: kShortPassError);
              }
              password = value;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kPassNullError);
                return kPassNullError;
              } else if (value.length < 8) {
                addError(error: kShortPassError);
                return kShortPassError;
              }
              return null;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              hintText: "Enter your password",
              hintStyle: GoogleFonts.ptSans(
                  textStyle: const TextStyle(color: fontColor)),
              // If  you are using latest version of flutter then lable text and hint text shown like this
              // if you r using flutter less then 1.20.* then maybe this is not working properly
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: const Icon(
                Icons.lock_outlined,
                color: fontColor,
              ),
              contentPadding: const EdgeInsets.only(
                left: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Column buildEmailFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Email",
          style:
              GoogleFonts.ptSans(textStyle: const TextStyle(color: fontColor)),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _email,
            keyboardType: TextInputType.emailAddress,
            onSaved: (newValue) => email = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kEmailNullError);
              } else if (emailValidatorRegExp.hasMatch(value)) {
                removeError(error: kInvalidEmailError);
              }
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kEmailNullError);
                return kEmailNullError;
              } else if (!emailValidatorRegExp.hasMatch(value)) {
                addError(error: kInvalidEmailError);
                return kInvalidEmailError;
              }
              return null;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
                hintText: "Enter your email",
                hintStyle: GoogleFonts.ptSans(
                    textStyle: const TextStyle(color: fontColor)),
                // If  you are using latest version of flutter then lable text and hint text shown like this
                // if you r using flutter less then 1.20.* then maybe this is not working properly
                floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon: const Icon(
                  Icons.email_outlined,
                  color: fontColor,
                ),
                contentPadding: const EdgeInsets.only(
                  left: 15,
                )),
          ),
        ),
      ],
    );
  }
}
