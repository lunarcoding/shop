import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

import 'components/body.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  // static String routeName = "/sign_up";
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: bgColor,
      body: Body(),
    );
  }
}
