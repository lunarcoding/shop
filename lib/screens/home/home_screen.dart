import 'package:flutter/material.dart';
import 'package:lunarcoding/components/card_shortview.dart';
import 'package:lunarcoding/components/cart_detailsview.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/components/navigation_drawer_widget.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/screens/home/components/discount_banner.dart';
import 'package:lunarcoding/screens/home/components/home_header.dart';
import 'package:lunarcoding/screens/home/components/popular.dart';
import 'package:lunarcoding/screens/home/components/healthy.dart';
import 'package:lunarcoding/screens/home/components/special_offers.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decode/jwt_decode.dart';
import 'dart:convert';

Future<Home> fetchHome(String token) async {
  var id = Jwt.parseJwt(token);
  var userid = ID.fromJson(id).user_id;
  final response =
      await http.get(Uri.parse('http://192.168.1.34:8080/users/$userid')
          // Send authorization headers to the backend.
          );
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Home.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load Home');
  }
}

class ID {
  // ignore: non_constant_identifier_names
  final int user_id;

  // ignore: non_constant_identifier_names
  ID({required this.user_id});

  factory ID.fromJson(Map<String, dynamic> json) {
    return ID(
      user_id: json['user_id'],
    );
  }
}

class Home {
  final int id;
  final String first_name;
  final String sur_name;
  final String phone;
  final String address;
  final String email;

  Home({
    required this.id,
    required this.first_name,
    required this.sur_name,
    required this.phone,
    required this.address,
    required this.email,
  });

  factory Home.fromJson(Map<String, dynamic> json) {
    return Home(
      id: json['id'],
      first_name: json['first_name'],
      sur_name: json['sur_name'],
      phone: json['phone'],
      address: json['address'],
      email: json['email'],
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, required this.token}) : super(key: key);
  final String token;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<Home> futureHome;

  @override
  void initState() {
    super.initState();
    futureHome = fetchHome(widget.token);
  }

  final bloc = HomeBLoC();

  void _onVerticalGesture(DragUpdateDetails details) {
    if (details.primaryDelta! < -0.7) {
      bloc.changeToCart();
    } else if (details.primaryDelta! > 0.7) {
      bloc.changeToNormal();
    }
  }

  double _getTopForWhitePanel(HomeState state, double maxHeight) {
    if (state == HomeState.normal) {
      return headerHeight;
    } else if (state == HomeState.cart) {
      return -(maxHeight - cartBarHeight * 2 - headerHeight);
    }
    return 0;
  }

  double _getTopForCartPanel(HomeState state, double maxHeight) {
    if (state == HomeState.normal) {
      return cartBarHeight;
    } else if (state == HomeState.cart) {
      return maxHeight - cartBarHeight;
    }
    return 0;
  }

  double _headerPosition(HomeState state) {
    if (state == HomeState.normal) {
      return 0;
    } else if (state == HomeState.cart) {
      return -headerHeight;
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: bloc,
      builder: (context, _) {
        return Scaffold(
            endDrawer: NavigationDrawerWidget(token: widget.token),
            backgroundColor: bgColor,
            body: SafeArea(
              bottom: false,
              child: Container(
                color: bgColor,
                child: Column(
                  children: [
                    Expanded(
                      child: LayoutBuilder(
                        builder: (context, BoxConstraints constraints) {
                          return Stack(
                            children: [
                              CartNav(constraints),
                              AnimatedPositioned(
                                duration: panelTransition,
                                curve: Curves.decelerate,
                                top: _getTopForWhitePanel(
                                    bloc.homeState, constraints.maxHeight),
                                left: 0,
                                right: 0,
                                height: constraints.maxHeight -
                                    cartBarHeight -
                                    headerHeight,
                                child: SingleChildScrollView(
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/Group 44.png"),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    child: Column(
                                      children: [
                                        const SpecialHome(),
                                        const SizedBox(height: 10),
                                        const DiscountBanner(),
                                        SpecialOffers(bloc: bloc,token:widget.token),
                                        const SizedBox(height: 30),
                                        const HealthyProducts(),
                                        const SizedBox(height: 30),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              FutureBuilder<Home>(
                                  future: futureHome,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return HeaderBar(
                                          snapshot.data!.first_name +
                                              ' ' +
                                              snapshot.data!.sur_name);
                                    } else if (snapshot.hasError) {
                                      return Text('${snapshot.error}');
                                    }

                                    // By default, show a loading spinner.
                                    return const CircularProgressIndicator();
                                  }),
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ));
      },
    );
  }

  AnimatedPositioned HeaderBar(name) {
    return AnimatedPositioned(
      duration: panelTransition,
      top: _headerPosition(bloc.homeState),
      left: 0,
      right: 0,
      child: HomeHeader(name: name),
    );
  }

  // ignore: non_constant_identifier_names
  AnimatedPositioned CartNav(BoxConstraints constraints) {
    return AnimatedPositioned(
      duration: panelTransition,
      curve: Curves.decelerate,
      bottom: 0,
      left: 0,
      right: 0,
      height: _getTopForCartPanel(bloc.homeState, constraints.maxHeight),
      child: GestureDetector(
        onVerticalDragUpdate: _onVerticalGesture,
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
            color: primaryColor,
          ),
          alignment: Alignment.topCenter,
          child: Padding(
            padding: const EdgeInsets.all(defaultPadding),
            child: AnimatedSwitcher(
              duration: panelTransition,
              child: bloc.homeState == HomeState.normal
                  ? CartShortView(bloc: bloc)
                  : CartDetailsView(bloc: bloc,token:widget.token),
            ),
          ),
        ),
      ),
    );
  }
}
