import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key, required this.name}) : super(key: key);
  final String name;
  @override
  Widget build(BuildContext context) {
    return Container(
      // สร้าง Container เพื่อทำการจัด layout
      height: 85, // ดึง headerHeight จาก constants.dart
      color: primaryColor,
      padding: const EdgeInsets.all(
          20), // จัด padding ตามค่า defaultPadding จาก constants.dart
      child: Row(
        // จัด layout เป็น Row
        mainAxisAlignment:
            MainAxisAlignment.spaceBetween, // จัดให้มีช่องว่าง ระหว่างกัน
        children: [
          Column(
            // จัด layout เป็น Column
            crossAxisAlignment:
                CrossAxisAlignment.start, // จัดให้อยู่ซ้ายของ Column
            children: [
              Text("Welcome",
                  style: GoogleFonts.ptSans(
                    textStyle: Theme.of(
                            context) // ดึง style จาก Theme ที่ตั้งไว้ใน main
                        .textTheme
                        .caption,
                  )),
              Text("Woratan Wongsathan",
                  style: GoogleFonts.ptSans(
                    textStyle: Theme.of(
                            context) // ดึง style จาก Theme ที่ตั้งไว้ใน main
                        .textTheme
                        .subtitle1! // ปรับขนาดตัวอักษร
                        .copyWith(color: Colors.black54),
                  )
                  // เปลี่ยนสีตัวอักษร
                  )
            ],
          ),
          const CircleAvatar(
            // ลองใช้ CircleAvatar ในการใส่รูปโปรไฟล์
            backgroundColor:
                Colors.transparent, // ทำให้พื้นหลังโปร่งใส(ไม่มีพื้นหลัง)
            backgroundImage:
                AssetImage("assets/images/profile.png"), // รูปโปรไฟล์
          )
        ],
      ),
    );
  }
}
