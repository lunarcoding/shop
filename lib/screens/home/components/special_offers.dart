import 'package:flutter/material.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/models/Product.dart';
import 'package:lunarcoding/screens/Vegetable/vegetable.dart';
import 'package:lunarcoding/screens/deatils/details_screen.dart';

import 'section_title.dart';

class SpecialOffers extends StatefulWidget {
  const SpecialOffers({
    Key? key,
    required this.bloc,
    required this.token,
  }) : super(key: key);

  final HomeBLoC bloc;
  final String token;

  @override
  State<SpecialOffers> createState() => _SpecialOffersState();
}

class _SpecialOffersState extends State<SpecialOffers> {
  int quantity = 1;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SectionTitle(
            title: "Vegetable",
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      VegetableScreen(bloc: widget.bloc, token: widget.token),
                ),
              );
            },
          ),
        ),
        const SizedBox(height: 20),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
          height: MediaQuery.of(context).size.height * 0.35,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: demo_products.length,
            itemBuilder: (context, index) => SizedBox(
              width: 150,
              child: SpecialOfferCard(
                image: demo_products[index].image!,
                category: demo_products[index].title!,
                numOfBrands: 18,
                press: () {
                  Navigator.of(context).push(
                    PageRouteBuilder(
                      transitionDuration: const Duration(milliseconds: 500),
                      reverseTransitionDuration:
                          const Duration(milliseconds: 600),
                      pageBuilder: (context, animation, secondaryAnimation) =>
                          FadeTransition(
                        opacity: animation,
                        child: DetailsScreen(
                          product: demo_products[index],
                          bloc: widget.bloc,
                          token: widget.token,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        )
        // SingleChildScrollView(
        // scrollDirection: Axis.horizontal,
        //   child: Row(
        //     children: [

        // SpecialOfferCard(
        //   image: "assets/images/Image Banner 2.png",
        //   category: "Smartphone",
        //   numOfBrands: 18,
        //   press: () {},
        // ),
        //       SpecialOfferCard(
        //         image: "assets/images/Image Banner 3.png",
        //         category: "Fashion",
        //         numOfBrands: 24,
        //         press: () {},
        //       ),
        //       SpecialOfferCard(
        //         image: "assets/images/Image Banner 3.png",
        //         category: "Fashion",
        //         numOfBrands: 24,
        //         press: () {},
        //       ),
        //       SpecialOfferCard(
        //         image: "assets/images/Image Banner 3.png",
        //         category: "Fashion",
        //         numOfBrands: 24,
        //         press: () {},
        //       ),
        //       const SizedBox(width: 20),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}

class SpecialOfferCard extends StatelessWidget {
  const SpecialOfferCard({
    Key? key,
    required this.category,
    required this.image,
    required this.numOfBrands,
    required this.press,
  }) : super(key: key);

  final String category, image;
  final int numOfBrands;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: GestureDetector(
        onTap: press,
        child: SizedBox(
          width: 130,
          height: 190,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Stack(
              children: [
                Hero(
                  tag: category,
                  child: Image.asset(image),
                ),
                // Image.asset(
                //   image,
                //   fit: BoxFit.cover,
                // ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFF343434).withOpacity(0.4),
                        Color(0xFF343434).withOpacity(0.15),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15.0,
                    vertical: 10,
                  ),
                  child: Text.rich(
                    TextSpan(
                      style: const TextStyle(color: Colors.white),
                      children: [
                        TextSpan(
                          text: "$category\n",
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
