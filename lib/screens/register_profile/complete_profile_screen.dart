import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

import 'components/body.dart';

class CompleteProfileScreen extends StatelessWidget {
  const CompleteProfileScreen({Key? key,required this.email,required this.password}) : super(key: key);
  final String email;
  final String password;

  // static String routeName = "/complete_profile";
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: bgColor,
      body: Body(email:email,password:password),
    );
  }
}
