import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';

import 'complete_profile_form.dart';

class Body extends StatelessWidget {
  const Body({Key? key,required this.email,required this.password}) : super(key: key);
  final String email;
  final String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/Group 44.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(height: 10),
                  Text(
                    "Complete Profile",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.ptSans(
                      textStyle: const TextStyle(
                        color: fontColor,
                        fontSize: 28,
                      ),
                    ),
                  ),
                  Text(
                    "Complete your details or continue  \nwith social media",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                      textStyle: const TextStyle(
                        color: fontColor,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                   CompleteProfileForm(email:email,password:password),
                  const SizedBox(height: 30),
                  Text(
                    "By continuing your confirm that you agree \nwith our Term and Condition",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.ptSans(
                      textStyle: Theme.of(context).textTheme.caption!.merge(
                            const TextStyle(
                              color: fontColor,
                            ),
                          ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
