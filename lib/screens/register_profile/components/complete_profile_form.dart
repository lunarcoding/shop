import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/components/custom_surfix_icon.dart';
import 'package:lunarcoding/components/default_button.dart';
import 'package:lunarcoding/components/form_error.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/screens/otp/otp_screen.dart';
import '../../../constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<Register> createRegister(
    String first_name,
    String sur_name,
    String phone,
    String address,
    String email,
    String password,
    context) async {
  final response = await http.post(
    Uri.parse('http://192.168.1.34:8080/users'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'first_name': first_name,
      'sur_name': sur_name,
      'phone': phone,
      'address': address,
      'email': email,
      'password': password,
    }),
  );
  // print(response.statusCode);
  if (response.statusCode == 201) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    // return Register.fromJson(jsonDecode(response.body));
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const OtpScreen(),
      ),
    );
    return Register.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create Register.');
  }
}

class Register {
  final int id;
  final String first_name;
  final String sur_name;
  final String phone;
  final String address;
  final String email;
  final String password;

  Register({
    required this.id,
    required this.first_name,
    required this.sur_name,
    required this.phone,
    required this.address,
    required this.email,
    required this.password,
  });

  factory Register.fromJson(Map<String, dynamic> json) {
    return Register(
      id: json['id'],
      first_name: json['first_name'],
      sur_name: json['sur_name'],
      phone: json['phone'],
      address: json['address'],
      email: json['email'],
      password: json['password'],
    );
  }
}

class CompleteProfileForm extends StatefulWidget {
  const CompleteProfileForm(
      {Key? key, required this.email, required this.password})
      : super(key: key);
  final String email;
  final String password;

  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  final TextEditingController _firstName = TextEditingController();
  final TextEditingController _lastName = TextEditingController();
  final TextEditingController _phoneNumber = TextEditingController();
  final TextEditingController _address = TextEditingController();
  Future<Register>? _futureRegister;

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? firstName;
  String? lastName;
  String? phoneNumber;
  String? address;

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildFirstNameFormField(),
          const SizedBox(height: 30),
          buildLastNameFormField(),
          const SizedBox(height: 30),
          buildPhoneNumberFormField(),
          const SizedBox(height: 30),
          buildAddressFormField(),
          FormError(errors: errors),
          const SizedBox(height: 40),
          SizedBox(
            width: 200,
            child: NeumorphicButton(
              style: NeumorphicStyle(
                boxShape: NeumorphicBoxShape.roundRect(
                  BorderRadius.circular(30),
                ),
                color: primaryColor,
                shape: NeumorphicShape.convex,
                depth: 10,
                intensity: 0.81,
              ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  // Navigator.pushNamed(context, OtpScreen.routeName);
                  setState(() {
                    _futureRegister = createRegister(
                        _firstName.text,
                        _lastName.text,
                        _phoneNumber.text,
                        _address.text,
                        widget.email,
                        widget.password,
                        context);
                  });
                }
              },
              child: Text(
                "continue",
                textAlign: TextAlign.center,
                style: GoogleFonts.ptSans(
                  textStyle: const TextStyle(
                    color: fontColor,
                    fontSize: 30,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column buildAddressFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Address",
          style:
              GoogleFonts.ptSans(textStyle: const TextStyle(color: fontColor)),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _address,
            onSaved: (newValue) => address = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kAddressNullError);
              }
              // ignore: avoid_returning_null_for_void
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kAddressNullError);
                return kAddressNullError;
              }
              return null;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              hintText: "Enter your phone address",
              hintStyle: GoogleFonts.ptSans(
                  textStyle: const TextStyle(color: fontColor)),
              // If  you are using latest version of flutter then lable text and hint text shown like this
              // if you r using flutter less then 1.20.* then maybe this is not working properly
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: const Icon(
                Icons.location_on_outlined,
                color: fontColor,
              ),
              contentPadding: const EdgeInsets.only(
                left: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Column buildPhoneNumberFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Phone Number",
          style:
              GoogleFonts.ptSans(textStyle: const TextStyle(color: fontColor)),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _phoneNumber,
            keyboardType: TextInputType.phone,
            onSaved: (newValue) => phoneNumber = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kPhoneNumberNullError);
              }
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kPhoneNumberNullError);
                return kPhoneNumberNullError;
              }
              return null;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              hintText: "Enter your phone number",
              hintStyle: GoogleFonts.ptSans(
                textStyle: const TextStyle(
                  color: fontColor,
                ),
              ),

              // If  you are using latest version of flutter then lable text and hint text shown like this
              // if you r using flutter less then 1.20.* then maybe this is not working properly
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: const Icon(
                Icons.phone_android,
                color: fontColor,
              ),
              contentPadding: const EdgeInsets.only(
                left: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Column buildLastNameFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Last Name",
          style: GoogleFonts.ptSans(
            textStyle: const TextStyle(
              color: fontColor,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _lastName,
            keyboardType: TextInputType.name,
            onSaved: (newValue) => lastName = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kLastNameNullError);
              } else if (nameValidatorRegExp.hasMatch(value)) {
                removeError(error: kInvalidLastNameError);
              }
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kLastNameNullError);
                return kLastNameNullError;
              } else if (!nameValidatorRegExp.hasMatch(value)) {
                addError(error: kInvalidLastNameError);
                return kInvalidLastNameError;
              }
              return null;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              hintText: "Enter your last name",
              hintStyle: GoogleFonts.ptSans(
                  textStyle: const TextStyle(color: fontColor)),

              // If  you are using latest version of flutter then lable text and hint text shown like this
              // if you r using flutter less then 1.20.* then maybe this is not working properly
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: const Icon(
                Icons.perm_identity,
                color: fontColor,
              ),
              contentPadding: const EdgeInsets.only(
                left: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Column buildFirstNameFormField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "First Name",
          style: GoogleFonts.ptSans(
            textStyle: const TextStyle(
              color: fontColor,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Neumorphic(
          style: NeumorphicStyle(
            color: bgColor,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(30)),
            depth: -8,
          ),
          child: TextFormField(
            controller: _firstName,
            onSaved: (newValue) => firstName = newValue,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kNamelNullError);
              } else if (nameValidatorRegExp.hasMatch(value)) {
                removeError(error: kInvalidFirstNameError);
              }
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kNamelNullError);
                return kNamelNullError;
              } else if (!nameValidatorRegExp.hasMatch(value)) {
                addError(error: kInvalidFirstNameError);
                return kInvalidFirstNameError;
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: "Enter your first name",
              // If  you are using latest version of flutter then lable text and hint text shown like this
              // if you r using flutter less then 1.20.* then maybe this is not working properly
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              hintStyle: GoogleFonts.ptSans(
                  textStyle: const TextStyle(color: fontColor)),

              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: const Icon(
                Icons.perm_identity,
                color: fontColor,
              ),
              contentPadding: const EdgeInsets.only(
                left: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
