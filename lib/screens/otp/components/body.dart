import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';
import 'otp_form.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/Group 44.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 50),
                Text(
                  "OTP Verification",
                  style: GoogleFonts.ptSans(
                    textStyle: headingStyle,
                  ),
                ),
                Text(
                  "We sent your code to +1 898 860 ***",
                  style: GoogleFonts.ptSans(
                      textStyle: const TextStyle(color: fontColor)),
                ),
                buildTimer(),
                const OtpForm(),
                const SizedBox(height: 5),
                GestureDetector(
                  onTap: () {
                    // OTP code resend
                  },
                  child: Text(
                    "Resend OTP Code",
                    style: GoogleFonts.ptSans(
                      textStyle: const TextStyle(
                        decoration: TextDecoration.underline,
                        color: fontColor,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Row buildTimer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "This code will expired in ",
          style: GoogleFonts.ptSans(
            textStyle: const TextStyle(
              color: fontColor,
            ),
          ),
        ),
        TweenAnimationBuilder(
          tween: Tween(begin: 30.0, end: 0.0),
          duration: const Duration(seconds: 30),
          builder: (_, dynamic value, child) => Text(
            "00:${value.toInt()}",
            style: const TextStyle(color: primaryColor),
          ),
        ),
      ],
    );
  }
}
