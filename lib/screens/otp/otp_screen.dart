import 'package:flutter/material.dart';
import 'package:lunarcoding/constants.dart';

import 'components/body.dart';

class OtpScreen extends StatelessWidget {
  const OtpScreen({Key? key}) : super(key: key);

  // static String routeName = "/otp";
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: bgColor,
      body: Body(),
    );
  }
}
