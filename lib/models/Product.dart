class Product {
  final String? title, image;
  final int? cost;

  Product({
    this.title,
    this.image,
    this.cost,
  });
}

List<Product> demo_products = [
  Product(title: "Cabbage", image: "assets/images/img_1.png", cost: 20),
  Product(title: "Broccoli", image: "assets/images/img_2.png", cost: 20),
  Product(title: "Carrot", image: "assets/images/img_3.png", cost: 20),
  Product(title: "Pakcoy", image: "assets/images/img_4.png", cost: 20),
  Product(title: "Cucumber", image: "assets/images/img_1.png", cost: 20),
];
