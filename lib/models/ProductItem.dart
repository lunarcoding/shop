import 'Product.dart';

class ProductItem {
  int quantity;
  final Product? product;

  ProductItem({required this.quantity, required this.product});

  void increment(int quantityold) {
    quantity = quantity + quantityold;
  }

  // void add() {}

  // void substract() {}
}
