import 'package:lunarcoding/models/Product.dart';
import 'package:lunarcoding/models/ProductItem.dart';
import 'package:flutter/material.dart';

enum HomeState { normal, details, cart }

class HomeBLoC with ChangeNotifier {
  HomeState homeState = HomeState.normal;

  List<ProductItem> cart = [];

  void changeToNormal() {
    homeState = HomeState.normal;
    notifyListeners();
  }

  void changeToCart() {
    homeState = HomeState.cart;
    notifyListeners();
  }

  void addProductToCart(Product product, int quantity) {
    for (ProductItem item in cart) {
      if (item.product!.title == product.title) {
        item.increment(quantity);
        notifyListeners();
        return;
      }
    }
    cart.add(ProductItem(product: product, quantity: quantity));
    notifyListeners();
  }

  int totalCartElements() => cart.fold(
      0, (previousValue, element) => previousValue + element.quantity);

  int totalCartCost() => cart.fold(
      0,
      (previousValue, element) =>
          previousValue + (element.quantity * element.product!.cost!));
}
