import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/screens/sign_in/sign_in_screen.dart';

class HaveAccountText extends StatelessWidget {
  const HaveAccountText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Already have an account? ",
          style: GoogleFonts.ptSans(textStyle: const TextStyle(fontSize: 16)),
        ),
        GestureDetector(
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => const SignInScreen())),
          child: Text(
            "Sign in",
            style: GoogleFonts.ptSans(
              textStyle: GoogleFonts.ptSans(
                textStyle: const TextStyle(fontSize: 16, color: fontColor),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
