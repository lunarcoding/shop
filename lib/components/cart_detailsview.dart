import 'package:flutter/cupertino.dart';
import 'package:lunarcoding/components/home_bloc.dart';
import 'package:lunarcoding/components/price.dart';
import 'package:lunarcoding/flutter_neumorphic.dart';
import 'package:lunarcoding/models/ProductItem.dart';
import 'package:flutter/material.dart';
import 'package:lunarcoding/screens/order/order.dart';

import '../../../constants.dart';

class CartDetailsView extends StatelessWidget {
  const CartDetailsView({
    Key? key,
    required this.bloc,
    required this.token,
  }) : super(key: key);

  final HomeBLoC bloc;
  final String token;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Basket",
            style: Theme.of(context)
                .textTheme
                .headline6!
                .copyWith(color: fontColor),
          ),
          ...List.generate(
            bloc.cart.length,
            (index) => CartDetailsViewCard(productItem: bloc.cart[index]),
          ),
          const SizedBox(height: defaultPadding),
          if (bloc.cart.isNotEmpty)
            SizedBox(
              width: double.infinity,
              child: NeumorphicButton(
                style: NeumorphicStyle(
                  boxShape: NeumorphicBoxShape.roundRect(
                    BorderRadius.circular(30),
                  ),
                  color: primaryColor,
                  shape: NeumorphicShape.convex,
                  depth: 10,
                  intensity: 0.81,
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>  OrderScreen(token:token,bloc:bloc)));
                },
                child: Text(
                  "Next - \$" + bloc.totalCartCost().toString(),
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: fontColor, fontSize: 30),
                ),
              ),
              // child: ElevatedButton(
              //   onPressed: () =>Navigator.push(context, MaterialPageRoute(builder: (context) =>const OrderScreen())),
              //   child: Text("Next - \$" + bloc.totalCartCost().toString()),
              // ),
            )
        ],
      ),
    );
  }
}

class CartDetailsViewCard extends StatelessWidget {
  const CartDetailsViewCard({
    Key? key,
    required this.productItem,
  }) : super(key: key);

  final ProductItem productItem;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(vertical: defaultPadding / 2),
      leading: CircleAvatar(
        radius: 25,
        backgroundColor: Colors.white,
        backgroundImage: AssetImage(productItem.product!.image!),
      ),
      title: Text(
        productItem.product!.title!,
        style: Theme.of(context)
            .textTheme
            .subtitle1!
            .copyWith(fontWeight: FontWeight.bold),
      ),
      trailing: FittedBox(
        child: Row(
          children: [
            const Price(amount: "20"),
            Text(
              "  x ${productItem.quantity}",
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
