import 'package:flutter/material.dart';
import 'package:lunarcoding/components/home_bloc.dart';

import '../../../constants.dart';

class CartShortView extends StatelessWidget {
  const CartShortView({
    Key? key,
    required this.bloc,
  }) : super(key: key);

  final HomeBLoC bloc;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "Basket",
          style:
              Theme.of(context).textTheme.headline6!.copyWith(color: fontColor),
        ),
        const SizedBox(width: defaultPadding),
        Expanded(
          // ทำให้เต็มพื้นที่
          child: SingleChildScrollView(
            // จัดให้เป็นเเบบเลื่อนได้เมื่อเกินความยาวหน้าจอ
            scrollDirection: Axis.horizontal, // จัดให้มีการเลื่อนเเบบเเนวนอน
            child: Row(
              // จัดให้เป็นการจัดเเบบ Row
              children: List.generate(
                // สร้าง List
                bloc.cart.length, // ใช้ลูปในการเเสดงสินค้าที่อยู่ในตะกร้า
                (index) => Padding(
                  // เซต Padding
                  padding: const EdgeInsets.only(right: defaultPadding / 2),
                  child: Hero(
                    // ทำการใช้อนิเมชั่น Hero
                    tag: bloc.cart[index].product!.title! + '_cart', // ชื่อ tag
                    child: CircleAvatar(
                      // ใช้ CircleAvatar ในการรองรับรูปสินค้าในตระกร้า
                      backgroundColor: Colors.white, //พื้นหลังขาว
                      backgroundImage: AssetImage(
                          bloc.cart[index].product!.image!), // ใส่รูปสินค้า
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        // ...List.generate(
        //   bloc.cart.length,
        //   (index) => Padding(
        //     padding: const EdgeInsets.only(right: defaultPadding / 2),
        //     child: Hero(
        //       tag: bloc.cart[index].product!.title! + '_cart',
        //       child: CircleAvatar(
        //         backgroundColor: Colors.white,
        //         backgroundImage: AssetImage(
        //           bloc.cart[index].product!.image!,
        //         ),
        //       ),
        //     ),
        //   ),
        // ),
        const SizedBox(width: 20),
        CircleAvatar(
          backgroundColor: Colors.white,
          child: Text(
            bloc.totalCartElements().toString(),
            style:
                const TextStyle(color: fontColor, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
