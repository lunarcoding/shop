import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lunarcoding/constants.dart';
import 'package:lunarcoding/screens/sign_up/sign_up_screen.dart';

class NoAccountText extends StatelessWidget {
  const NoAccountText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Don’t have an account? ",
          style: GoogleFonts.ptSans(
            textStyle: const TextStyle(fontSize: 16),
          ),
        ),
        GestureDetector(
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => const SignUpScreen())),
          child: Text(
            "Sign Up",
            style: GoogleFonts.ptSans(
                textStyle: const TextStyle(fontSize: 16, color: fontColor)),
          ),
        ),
      ],
    );
  }
}
