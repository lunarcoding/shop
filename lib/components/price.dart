import 'package:flutter/material.dart';

import '../constants.dart';

class Price extends StatelessWidget {
  const Price({
    Key? key,
    required this.amount,
  }) : super(key: key);
  final String amount;

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        text: "฿",
        style: Theme.of(context).textTheme.subtitle1!.copyWith(
            fontWeight: FontWeight.w600, color: const Color(0xFF54EE6D)),
        children: [
          TextSpan(
            text: amount,
            style: const TextStyle(color: fontColor),
          ),
          const TextSpan(
            text: "/kg",
            style:
                TextStyle(color: Colors.black26, fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
  }
}
