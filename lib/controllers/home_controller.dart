import 'package:flutter/material.dart';
import 'package:lunarcoding/models/Product.dart';
import 'package:lunarcoding/models/ProductItem.dart';

enum HomeState { normal, cart } // กำหนดให้ HomeState มี 2 สถานะ

class HomeController extends ChangeNotifier {
  HomeState homeState = HomeState.normal; // ให้ homeState = HomeState.normal

  List<ProductItem> cart = []; // สร้าง List เก็บ สินค้า

  void changeHomeState(HomeState state) {
    // ฟังชั่นเปลี่ยน สถานะตามที่รับมา
    homeState = state;
    notifyListeners();
  }

  void addProductToCart(Product product,int quantity) {
    // ฟังชั่นรับค่าสินค้า
    for (ProductItem item in cart) {
      if (item.product!.title == product.title) {
        // ถ้า ชื่อสินค้าใน ProductItem == ชื่อสินค้าที่รับมา ให้ทำการเพิ่มจำนวน
        item.increment(quantity);
        notifyListeners();
        return;
      }
    }
    cart.add(ProductItem(product: product,quantity:quantity)); // รับสินค้าเข้าตะกร้า
    notifyListeners();
  }

  int totalCartItems() => cart.fold(
      // ตั้งค่าจำนวนสินค้าทั้งหมด
      0,
      (previousValue, element) => previousValue + element.quantity);
}
